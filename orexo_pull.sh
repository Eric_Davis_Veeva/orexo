#!/bin/sh

# Author: Eric Davis
# eric.davis@veeva.com

# This script is responsible for pulling and sending the weekly DEA updates for Orexo

basedir="/mnt/data/opendata"
logs="{$basedir}/logs/ftp.log"
REMOTEDIR="DW DEA Total List of Records*"
REPORTDIR="DW DEA Total List of Records_[0-9]+_[0-9]+"
REPORTFILE="DW DEA Total List of Records_1.csv"

REMOTEPATH="/outbound/report/"
FILE="full_export_[0-9T]+\.zip"
USER=$(cat ${basedir}/env/env_conn | grep "usm_network|"|sed -n -e 's/usm_network|//p')
HOST=$(cat ${basedir}/env/env_conn | grep "usm_network|"|sed -n -e 's/.*@//p'|sed -n -e 's/,.*//p')
dirfile="dirfile"
email="eric.davis@veeva.com,gene.padilla@veeva.com"

echo $USER
echo $HOST


#Get directory listing of for the report directories
lftp -e "set ssl:verify-certificate false; set ftp:ssl-force true; set ftp:ssl-protect-data true; cd $REMOTEPATH ; ls -Art $REMOTEDIR | tail -n 1; bye;" -u $USER $HOST > ${dirfile}
LATEST="$(cat $dirfile | egrep -o "$REPORTDIR")"

echo "LATEST IS : "$LATEST
PULL="$REMOTEPATH$LATEST/"
echo $PULL
echo $REPORTFILE

# using LATEST, grab the actual directory and file from network directory
lftp -e "set ssl:verify-certificate false; set ftp:ssl-force true; set ftp:ssl-protect-data true; set xfer:clobber on; cd \"$PULL\" ; get \"$REPORTFILE\" ; bye;" -u $USER $HOST
echo 'Working with ' "$REPORTFILE"

# Column should be "vid__v"
echo '"vid__v"' > adhoc_opendata_download_vids.csv

# Cut the first column and remove header to final output file
cut -d',' -f1 "$REPORTFILE" | tail -n +2 >> adhoc_opendata_download_vids.csv

# Email the result
# echo 'Weekly Orexo Orexo VIDs attached' | mailx -a adhoc_opendata_download_vids.csv -s "Orexo Test" eric.davis@veeva.com,gene.padilla@veeva.com
 
 
echo 'Weekly Orexo VIDs' | mailx -a adhoc_opendata_download_vids.csv -s "[PROD] Orexo weekly email" -S smtp=smtp://smtp.mailgun.org:587 -S smtp-auth=login -S smtp-auth-user=opendataprod@veeva.com -S smtp-auth-password=YllPD3mrhyw0UVhB2pmsUmXSOiZCS4wU -S from="ODS_PROD <admin@veevaopendata.com>" $email
 rm adhoc_opendata_download_vids.csv
 
rm "$REPORTFILE"


exit
